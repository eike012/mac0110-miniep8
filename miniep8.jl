using Test
function testCompare()
    @test compareByValue("5", "8") == true
    @test compareByValue("A", "K") == false
    @test compareByValue("A", "A") == false
    @test compareByValue("6", "J") == true
    @test compareByValue("Q", "K") == true
    @test compareByValue("K", "J") == false
    @test compareByValue("1", "A") == true
    @test compareByValueAndSuit("2♢", "2♣") == true
    @test compareByValueAndSuit("6♠", "8♡") == true
    @test compareByValueAndSuit("A♡", "A♣") == true
    @test compareByValueAndSuit("J♠", "K♢") == false
    @test compareByValueAndSuit("Q♣", "Q♠") == false
    @test compareByValueAndSuit("A♡", "A♢") == false
    @test compareByValueAndSuit("A♣", "A♢") == false
    println("Fim dos testes")
end

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValue(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function sndinsercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function compareByValue(x, y)
    if x == "Q" && y == "A"
        return true
    end

    if x == "A" && y == "Q"
        return false
    end

    if x == "K" && y == "A"
        return true
    end

    if x == "A" && y == "K"
        return false
    end

    if x == "J" && y == "A"
        return true
    end

    if x == "A" && y == "J"
        return false
    end

    if x == "Q" && y == "K"
        return true
    end

    if x == "K" && y == "Q"
        return false
    end

    if x < y
        return true
    else
        return false
    end
end

function compareByValueAndSuit(x, y)
    valor = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K","A"]
    naipe = ["♢", "♠", "♡", "♣"]
    cartas = []
    n = 0
    m = 0

    for i in 1:length(naipe)
        for j in 1:length(valor)
            push!(cartas, valor[j]*naipe[i])
        end
    end

    for i in 1:length(cartas)
        if x == cartas[i]
            n = i
        end
        if y == cartas[i]
            m = i
        end
    end

    if n < m
        return true
    else
        return false
    end
end

testCompare()